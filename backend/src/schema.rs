// @generated automatically by Diesel CLI.

diesel::table! {
    accounts (id) {
        id -> Int8,
        #[max_length = 32]
        username -> Varchar,
        role -> Int2,
    }
}

diesel::table! {
    communities (id) {
        id -> Int8,
        #[max_length = 32]
        name -> Varchar,
        #[max_length = 126]
        description -> Varchar,
    }
}

diesel::table! {
    community_members (communityid, accountid) {
        communityid -> Int8,
        accountid -> Int8,
        role -> Int2,
    }
}

diesel::joinable!(community_members -> accounts (accountid));
diesel::joinable!(community_members -> communities (communityid));

diesel::allow_tables_to_appear_in_same_query!(
    accounts,
    communities,
    community_members,
);
