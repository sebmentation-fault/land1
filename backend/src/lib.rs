#![feature(proc_macro_hygiene, decl_macro)]
use diesel::pg::PgConnection;
use diesel::prelude::*;
use dotenvy::dotenv;
use std::env;

pub mod models;
pub mod schema;
pub mod routes;
pub mod services;

/// An enumeration for the different users of the entire system.
///
/// Increasing level of privileges; therefore, a user has less privileges than a system admin, and the admin has less privileges than the developer of the system.
#[derive(Copy, Clone)]
pub enum SystemRole {
    User = 0,
    LowLevelStaff = 1,
    HighLevelStaff = 2,
    Admin = 3,
    Developer = 4
}

/// Library function from casting an `int` to a `SystemRole`
impl SystemRole {
    pub fn from_i16(value: i16) -> SystemRole {
        match value {
            0 => SystemRole::User,
            1 => SystemRole::LowLevelStaff,
            2 => SystemRole::HighLevelStaff,
            3 => SystemRole::Admin,
            4 => SystemRole::Developer,
            _ => panic!("Unknown value: {}", value),
        }
    }
}

/// An enumeration for the different levels of access that each member of a Community may have,
/// assuming that they are whitelisted. 
///
/// Admins of a place have greater access to manipulate certain details than other users of the
/// place.
#[derive(Copy, Clone)]
pub enum CommunityRole {
    MEMBER = 0,
    Admin = 1
}

/// Library function from casting an `int` to a `CommunityRole`
impl CommunityRole {
    pub fn from_i16(value: i16) -> CommunityRole {
        match value {
            0 => CommunityRole::MEMBER,
            1 => CommunityRole::Admin,
            _ => panic!("Unknown value: {}", value),
        }
    }
}

/// Library function to establish a `PgConnection`
///
/// Throws an error message, and crashes, if there is an error connecting to the database
///
/// # Examples
///
/// ```
/// let connection = &mut backend::establish_connection();
/// ```

pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
}
