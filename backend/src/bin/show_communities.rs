use crate::models::*;
use diesel::prelude::*;
use backend::*;

fn main() {
    use crate::schema::communities::dsl::*;

    let connection: &mut PgConnection = &mut establish_connection();
    let results: Vec<Community> = communities
        .select(Community::as_select())
        .load(connection)
        .expect("Error loading communities");

    println!("Listing {} communities", results.len());
    for community in results {
        println!("\n\n{}", community.name);
        println!("-----------\n");
        println!("id: {}", community.id);
        println!("description: {}", community.description);

        use crate::schema::community_members::dsl::*;

        let whitelist: Vec<CommunityMembers> = community_members
            .filter(communityid.eq(community.id))
            .select(CommunityMembers::as_select())
            .load(connection)
            .expect("Error loading whitelist");

        for w in whitelist {
            use self::schema::accounts::dsl::*;

            let members = accounts
                .filter(id.eq(w.accountid))
                .select(Account::as_select())
                .load(connection)
                .expect("Error loading account");
            println!("Member: {}, role: {}", members[0].username, w.role);
        }
    }
}
