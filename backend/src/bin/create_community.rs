use backend::*;
use std::io::stdin;

use backend::services::communities::create_community;

fn main() {
    let connection = &mut establish_connection();

    let mut founder = String::new();
    let mut name = String::new();
    let mut desc = String::new();

    println!("Who is the founder (input id)?");
    stdin().read_line(&mut founder).unwrap();
    let founder_id: i64 = founder.trim_end().parse().expect("Input not an integer");

    println!("Give your community a name:");
    stdin().read_line(&mut name).unwrap();
    let name: &str = name.trim_end();

    println!("Give your community a description:");
    stdin().read_line(&mut desc).unwrap();
    let desc: &str = desc.trim_end();

    let community: Option<models::Community> = create_community(
        connection, founder_id, name.to_string(), desc.to_string()
    );
    if community.is_some() {
        println!("\nSaved community with id {}", community.unwrap().id);
    }
    else {
        println!("\nThe community could not be created")
    }
    
}

