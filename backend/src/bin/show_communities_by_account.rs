use crate::models::{Community, CommunityMembers};
use diesel::prelude::*;
use std::{env, process::ExitCode};
use backend::*;

fn main() -> ExitCode {
    use crate::schema::community_members::dsl::*;

    // sanitize user id
    let args: Vec<String> = env::args().collect();
    let user_id: Option<&String> = args.last();
    
    if user_id.is_none() {
        println!("User ID is required.");
        return ExitCode::FAILURE;
    }

    let user_id: &String = user_id.unwrap();

    if !user_id.chars().all(|c| c.is_numeric()) {
        println!("User ID must be a positive integer.");
        return ExitCode::FAILURE;
    }

    // convert user id to integer
    let user_id: i64 = user_id.parse().unwrap();

    let connection: &mut PgConnection = &mut establish_connection();

    // filter community_members for only ones by ID
    let whitelisted_communities: Vec<CommunityMembers> = community_members
        .filter(accountid.eq(user_id))
        .select(CommunityMembers::as_select())
        .load(connection)
        .expect("Error loading whitelist of community members.");

    if whitelisted_communities.len() == 0 {
        println!("You are not a member of any communities.");
        return ExitCode::SUCCESS;
    }

    println!("Listing {} communities that you are a member of.", whitelisted_communities.len());

    let mut index = 1;

    for whitelisted_community in whitelisted_communities {
        let comunity_id = whitelisted_community.communityid;

        use crate::schema::communities::dsl::*;

        let community: Community = communities
            .filter(id.eq(comunity_id))
            .select(Community::as_select())
            .first(connection)
            .expect("Error loading community");
        
        println!("\nCommunity {}: {}", index,  community.name);
        index += 1;
    }

    ExitCode::SUCCESS
}
