use backend::*;
use std::io::stdin;

fn main() {
    let connection = &mut establish_connection();

    let mut username = String::new();
    let mut role = String::new();

    println!("What would you like your username to be?");
    stdin().read_line(&mut username).unwrap();
    let username = username.trim_end();

    println!("What is your role (input integer from SystemRole)?");
    stdin().read_line(&mut role).unwrap();
    let role_i16: i16 = role.trim_end().parse().expect("Input not an integer");
    let role = SystemRole::from_i16(role_i16);

    let account = crate::services::accounts::create_account(connection, username.to_string(), role);

    if account.is_some() {
        println!("\nSaved account with id {}", account.unwrap().id);
    }
}

