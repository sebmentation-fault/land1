use crate::models::*;
use diesel::prelude::*;
use backend::*;

fn main() {
    use crate::schema::accounts::dsl::*;

    let connection = &mut establish_connection();
    let results = accounts
        .select(Account::as_select())
        .load(connection)
        .expect("Error loading accounts");

    println!("Listing {} accounts", results.len());
    for account in results {
        println!("\n\n{}", account.username);
        println!("-----------\n");
        println!("id: {}", account.id);
        println!("role: {}", account.role);
    }
}
