use crate::models::{Community, CommunityMembers};

use diesel::pg::PgConnection;
use diesel::prelude::*;

/// Library function to fetch all the communities that a user is a member of
///
/// # Arguments
///
/// * `conn` a mutable by reference `PgConnection` to the PostgresSQL database
/// * `account_id` the id for the member
///
/// # Examples
///
/// ```
/// let connection = &mut backend::establish_connection();
/// let account_id = 123456;
/// 
/// MaybeCommunities = backend::services::get_communities_by_account_id(connection, account_id);
/// ```
pub fn get_communities_by_account_id(
    conn: &mut PgConnection,
    account_id: i64
) -> Result<Vec<Community>, String> {
    use crate::schema::communities::dsl::*;
    use crate::schema::community_members::dsl::*;

    // sanitize account id
    if account_id.is_negative() {
        return Err("Account ID is not valid".to_string());
    }

    // filter community_members for only ones by ID
    let whitelisted_communities: Vec<CommunityMembers> = community_members
        .filter(accountid.eq(account_id))
        .select(CommunityMembers::as_select())
        .load(conn)
        .expect("Error loading whitelist of community members.");

    // no communities that the user is a member of 
    if whitelisted_communities.len() == 0 {
        return Err("Account ID matches 0 communities".to_string());
    }

    let mut accounts_communities: Vec<Community> = Vec::new();

    // fetch the community details
    for whitelisted_community in whitelisted_communities {
        let community_id = whitelisted_community.communityid;

        let community: Community = communities
            .filter(id.eq(community_id))
            .select(Community::as_select())
            .first(conn)
            .expect("Error loading community that account is a member of");

        accounts_communities.push(community);
    }

    if accounts_communities.len() == 0 {
        return Err("Account ID matched communities but failed to fetch it's details".to_string());
    }

    Ok(accounts_communities)
}