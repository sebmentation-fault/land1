use diesel::PgConnection;
use diesel::prelude::*;

use crate::models::{Community, CommunityMembers, NewCommunity, NewCommunityMember};
use crate::CommunityRole;

/// Library function to create a new community
///
/// # Arguments
///
/// * `conn` a mutable by reference `PgConnection` to the PostgreSQL database
/// * `id` the integer identifier for the account that is creating the place, and will be assigned to an admin automatically
/// * `name` a name for the community (max 32 characters)
/// * `description` a description for the community (max 126 characters)
///
/// # Examples
///
/// ```
/// let connection = &mut backend::establish_connection();
/// let community_founder: i64 = 12345678;
/// let community_name: String = "My Community";
/// let community_description: String = "A Community for me and my friends!";
///
/// backend::create_community(connection, community_founder, community_name, community_description);
/// ```
pub fn create_community(
    conn: &mut PgConnection, 
    id: i64,
    name: String,
    description: String) -> Option<Community> {

    use crate::schema::communities;
    use crate::schema::community_members;

    // name is not alphanumeric
    if !name.chars().all(|x| x.is_ascii()) {
        return None;
    }

    // description is not alphabetic
    if !description.chars().all(|x| x.is_ascii()) {
        return None;
    }

    // account id does not exist
    // TODO: create service to check account

    let new_community = NewCommunity { name, description };

    // create a community record in the commmunities table
    println!("Trying to create community record...");
    let community = diesel::insert_into(communities::table)
        .values(&new_community)
        .returning(Community::as_returning())
        .get_result(conn)
        .expect("Error creating a new community");
    println!("Done!");

    let communityid: i64 = community.id;
    let accountid: i64 = id;
    let role: i16 = CommunityRole::Admin as i16;
    let new_community_member = NewCommunityMember { communityid, accountid, role};

    // add the founder into the community_members table
    println!("Trying to create whitelist record...");
    diesel::insert_into(community_members::table)
        .values(&new_community_member)
        .returning(CommunityMembers::as_returning())
        .get_result(conn)
        .expect("Error saving community founder");
    println!("Done!");

    return Some(community);
}


#[cfg(test)]
mod tests {

    #[test]
    fn create_community_name_valid() {
        let connection: &mut diesel::PgConnection = &mut crate::establish_connection();
        let founder_id: i64 = 1;
        let name: String = "My Community".to_string();
        let desc: String = "Community of close friends!".to_string();

        let community: Option<crate::models::Community> = crate::services::communities::create_community(connection, founder_id, name, desc);
        assert_eq!(community.is_some(), true);
    }

}
