use crate::models::{Account, NewAccount};
use crate::SystemRole;

use diesel::pg::PgConnection;
use diesel::prelude::*;

/// Library function to create an account.
///
/// Validates data, and returns `None` if an account cannot be created.
///
/// # Arguments
///
/// * `conn` a mutable by reference `PgConnection` to the PostgreSQL database
/// * `username` a `String` holding the username that the user wants to be identified by
///     if the `username` contains non-ascii characters or contains any ascii-control characters,
///     then `None` is returned
/// * `role` an enumeration 
///
/// # Examples
///
/// ```
/// let connection = &mut backend::establish_connection();
/// let username = "sebmentationfault".to_string();
/// let role = backend::SystemRole::User;
///
/// backend::create_account(connection, username, role);
/// ```
pub fn create_account(conn: &mut PgConnection, 
    username: String, 
    role: SystemRole) -> Option<Account> {
    use crate::schema::accounts;

    // username is not alphanumeric
    if !username.chars().all(|x| x.is_ascii() && !x.is_ascii_control()) {
        return None;
    }

    let role = role as i16;
    let new_account = NewAccount { username, role };

    Some(diesel::insert_into(accounts::table)
        .values(&new_account)
        .returning(Account::as_returning())
        .get_result(conn)
        .expect("Error saving new account"))
}



#[cfg(test)]
mod tests {
    use diesel::pg::PgConnection;

    #[test]
    fn create_account_username_valid() {
        let connection: &mut PgConnection = &mut crate::establish_connection();
        let username: String = "sebmentationfault".to_string();
        let role = crate::SystemRole::User;

        let account: Option<crate::models::Account> = crate::services::accounts::create_account(connection, username, role);
        assert_eq!(account.is_some(), true);
    }

    #[test]
    fn create_account_username_invalid() {
        let connection: &mut PgConnection = &mut crate::establish_connection();
        let username: String = "non{alpha+numeric".to_string();
        let role: crate::SystemRole = crate::SystemRole::User;

        let account: Option<crate::models::Account> = crate::services::accounts::create_account(connection, username, role);
        assert_eq!(account.is_none(), true);
    }

}