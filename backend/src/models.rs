use crate::schema::accounts;
use crate::schema::communities;
use crate::schema::community_members;
use diesel::prelude::*;
use serde::{Serialize, Deserialize};

#[derive(Queryable, Selectable, Serialize, Deserialize)]
#[diesel(table_name = accounts)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Account {
    pub id: i64,
    pub username: String,
    pub role: i16,
}

#[derive(Queryable, Selectable, Serialize, Deserialize)]
#[diesel(table_name = communities)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Community {
    pub id: i64,
    pub name: String,
    pub description: String,
}

#[derive(Queryable, Selectable, Serialize, Deserialize)]
#[diesel(table_name = community_members)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct CommunityMembers {
    pub communityid: i64,
    pub accountid: i64,
    pub role: i16,
}

#[derive(Insertable)]
#[diesel(table_name = accounts)]
pub struct NewAccount {
    pub username: String,
    pub role: i16,
}

#[derive(Insertable)]
#[diesel(table_name = communities)]
pub struct NewCommunity {
    pub name: String,
    pub description: String,
}

#[derive(Insertable)]
#[diesel(table_name = community_members)]
pub struct NewCommunityMember {
    pub communityid: i64,
    pub accountid: i64,
    pub role: i16,
}
