#![feature(proc_macro_hygiene, decl_macro)]

extern crate rocket;

use std::process::ExitCode;
use rocket::error::LaunchError;

use dotenvy::dotenv;
use backend::routes::get_all_routes;

fn main() -> ExitCode {

    // ensure that `.env` exists
    if dotenv().ok().is_none() {
        println!("`.env` file could not be found.\nApplication will not be started.");
        return ExitCode::FAILURE;
    }
    
    // launch the rocket
    let launch_error: LaunchError = rocket::ignite()
        .mount("/api", get_all_routes())
        .launch();

    drop(launch_error);

    ExitCode::SUCCESS
}
