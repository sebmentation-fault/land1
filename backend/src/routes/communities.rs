use crate::models::Community;
use crate::{establish_connection, services};

use rocket::{get, routes, Route, data};
use rocket::http::RawStr;
use rocket_contrib::json::Json;
use diesel::pg::PgConnection;


pub fn get_routes() -> Vec<Route> {
    return routes![
        get_communities_by_user_id,
    ];
}

// unsecure function
// should change to using a user authenticator not set id the whole time
#[get("/users/<userid>/communities")]
pub fn get_communities_by_user_id(userid: &RawStr) -> Json<Result<Vec<Community>, String>> {

    // parse into int
    let maybe_id: Result<i64, _> = userid.to_string().parse();
    if maybe_id.is_err() {
        return Json(Err("User ID could not be passed to an i64".to_string()));
    }

    let id: i64 = maybe_id.unwrap();

    if id.is_negative() {
        return Json(Err("User ID is not allowed to be negative".to_string()));
    }

    let connection: &mut PgConnection = &mut establish_connection();

    let communities: Result<Vec<Community>, String> = services::community_members::get_communities_by_account_id(
        connection, 
        id
    );

    // if there are no communities for the account return nothing
    if communities.is_err() {
        return Json(Err("User ID matched no communities".to_string()));
    }

    let communities: Vec<Community> = communities.unwrap();

    return Json(Ok(communities));
}
