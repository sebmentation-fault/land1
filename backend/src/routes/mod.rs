pub mod log_in;
pub mod communities;

use rocket::Route;

/// Gets all the availiable routes within the module
pub fn get_all_routes() -> Vec<Route> {
    let mut route_vec: Vec<Route> = Vec::new();
    
    // for all modules, append get_routes
    route_vec.append(&mut log_in::get_routes());
    route_vec.append(&mut communities::get_routes());

    return route_vec;
}
