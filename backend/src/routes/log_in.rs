use rocket::{post, routes, Route};
use rocket::http::RawStr;

pub fn get_routes() -> Vec<Route> {
    return routes![
        log_in,
        register,
    ];
}

#[post("/log_in/<username>/<password>")]
pub fn log_in(username: &RawStr, password: &RawStr) -> String {
    "Log in info".to_string()
}

#[post("/register/<username>/<password>")]
pub fn register(username: &RawStr, password: &RawStr) -> String {
    "Request register".to_string()
}
