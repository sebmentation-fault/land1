CREATE TABLE communities (
    id           BIGSERIAL PRIMARY KEY,
    name         VARCHAR(32) NOT NULL,
    description  VARCHAR(126) NOT NULL
);
