CREATE TABLE community_members (
    communityid BIGINT NOT NULL REFERENCES communities(id),
    accountid   BIGINT NOT NULL REFERENCES accounts(id),
    role        SMALLINT NOT NULL DEFAULT 0,
    PRIMARY KEY (communityid, accountid)
);
