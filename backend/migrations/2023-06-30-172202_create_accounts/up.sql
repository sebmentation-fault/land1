CREATE TABLE accounts (
    id           BIGSERIAL PRIMARY KEY,
    username     VARCHAR(32) NOT NULL,
    role         SMALLINT NOT NULL DEFAULT 0
);
