# Database Setup

Read the SRS for specification of each table. This document includes code to create the database, useful for when loaded onto a new machine.

```bash
$ createdb land1
```

By default listens to port 5432.


## Land1 Server Setup

```sql
CREATE TABLE account (
    id           BIGSERIAL PRIMARY KEY,
    username     VARCHAR{32},
    role         SMALLINT
);

CREATE TABLE accountcredentials (
    id           BIGINT PRIMARY KEY REFERENCES account(id),
    hashpassword VARCHAR(64),
    saltvalue    VARCHAR(16)
);

CREATE TABLE places (
    id           BIGSERIAL PRIMARY KEY,
    placename    VARCHAR(32),
    placecountry VARCHAR(32),
    placeregion  VARCHAR(32),
    placeaddress VARCHAR(255),
    placeadmin   BIGINT REFERENCES account(id)
);

CREATE TABLE whitelists (
    placeid      BIGINT REFERENCES places(id),
    trustedid    BIGINT REFERENCES account(id),
    PRIMARY KEY (placeid, trustedid)
);

CREATE TABLE events (
    eventid      BIGSERIAL PRIMARY KEY,
    placeid      BIGINT REFERENCES places(id),
    estatus      SMALLINT,
    title        VARCHAR(64),
    edescription VARCHAR(256),
    starttime    TIMESTAMP WITH TIME ZONE, -- yyyy-mm-dd hh:MM:ss +zone,
    endtime      TIMESTAMP WITH TIME ZONE
);

CREATE TABLE projects (
    projectid    BIGSERIAL PRIMARY KEY,
    placeid      BIGINT REFERENCES places(id),
    pstatus      SMALLINT,
    pdescription VARCHAR(256),
);
```
