# Abstract

A unified web-application that let's user's find dates availiable to stay at Skogsbo, find dates that are availiable to take the boats out, and provide a list of projects that people are working on and may want help for.

# Documentation

Documentation can be found by navigating to the [docs directory](./docs/) in the GitLab repository.

The PDFs can be accessed directly, below:

* [Vision and Scope](./docs/VisionAndScope.pdf)
* [Elicitation and Analysis](./docs/ElicitationAndAnalysis.pdf)
* [Software Requirements Specification](./docs/SoftwareRequirementsSpecification.pdf)

# Processes

## Frontend

The frontend uses Angular. Files can be found in the [land1 directory](./land1/) of this repository.

## Database

PostgreSQL is used to host a database.

The database name is also `land1`.

A separate database is used for user authentication. This database is called `authenticate_land1_db`.